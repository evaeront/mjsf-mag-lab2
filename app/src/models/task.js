export class Task {
  static TS_COMPLETED = 'completed'
  static TS_INCOMPLETED = 'incompleted'
  static PRIORITY_LIST = {
    urgent: {text: 'Urgent', color: 'red'},
    important: {text: 'Important', color: 'orange'},
    normal: {text: 'Normal', color: 'green'},
    minor: {text: 'Minor', color: 'blue'}
  }

  constructor() {
    this.title = "";
    this.description = "";
    this.status = Task.TS_INCOMPLETED;
    this.taskPriority = "";
  }


}
